import * as cdk from "@aws-cdk/core"
import * as codepipeline from "@aws-cdk/aws-codepipeline"
import { CdkPipeline, SimpleSynthAction } from "@aws-cdk/pipelines";
import * as codepipeline_actions from '@aws-cdk/aws-codepipeline-actions';
import * as codecommit from '@aws-cdk/aws-codecommit';


export class S3PipelineStack extends cdk.Stack {
    constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
        super(scope, id, props);

        const sourceArtifact = new codepipeline.Artifact()

        const cloudAssemblyArtifact = new codepipeline.Artifact()

        const repo = new codecommit.Repository(this, 'Repo', {
            repositoryName:'msh'
          });

        const pipeline = new CdkPipeline(this, 'Pipeline', {
            pipelineName: 'S3pipelines',
            cloudAssemblyArtifact,
            sourceAction : new codepipeline_actions.CodeCommitSourceAction({
                actionName: 'CodeCommit',
                repository: repo,
                output: sourceArtifact,
              }),
            synthAction : SimpleSynthAction.standardNpmSynth({
                  sourceArtifact,
                  cloudAssemblyArtifact, 
                  buildCommand : 'npm run build'  
            })  
        })

    }

}