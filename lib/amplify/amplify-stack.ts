import * as cdk from '@aws-cdk/core';
import * as amplify from '@aws-cdk/aws-amplify'

export class AmplifyPipeStack extends cdk.Stack {
  constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    const mshapp = new amplify.App(this, 'mshapp', {
      sourceCodeProvider: new amplify.GitLabSourceCodeProvider({
        owner: "agurha",
        repository: "msh",
        oauthToken: cdk.SecretValue.secretsManager("gitlab", {
          jsonField: "gitlab",
        }),
      }), 
    });
    const masterBranch = mshapp.addBranch('master')
    const featureBranch = mshapp.addBranch('feature')
  }
}
